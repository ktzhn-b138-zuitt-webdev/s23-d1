// CRUD Operations
/*  
	Create
	Read
	Update
	Delete
*/

// Inserting Documents (Create)
// SYNTAX: db.collectionName.insertOne({object});
// JAVASCRIPT: object.object.method({object});

db.users.insertOne({
        "firstName": "Jane", 
        "lastName": "Doe",
        "age": 21,
        "contact": {
            "phone": "09192382320",
            "email": "janedoe@mail.com"
        },
        "courses": ["CSS", "Javascript", "Python"],
        "department": "none"
    });

db.users.insertOne({
        "firstName": "John", 
        "lastName": "Doe",
        "age": 25,
        "contact": {
            "phone": "0918763220",
            "email": "johndoe@mail.com"
        },
        "courses": ["CSS", "Bootstrap", "Python"],
        "department": "none"
    });
// Insert Many
// SYNTAX: db.collectionName.insertMany({objectA}, {objectB});
db.users.insertMany([
{
        "firstName": "Stephen", 
        "lastName": "Hawking",
        "age": 76,
        "contact": {
            "phone": "09143244235",
            "email": "stephenhawking@mail.com"
        },
        "courses": ["React", "PHP", "Python"],
        "department": "none"
},
{
        "firstName": "Neil", 
        "lastName": "Armstrong",
        "age": 82,
        "contact": {
            "phone": "09063198477",
            "email": "neilarmstrong@mail.com"
        },
        "courses": ["React", "Laravel", "SASS"],
        "department": "none"
}
]);

// Finding documents (Read) Operation 
/* FIND SYNTAX: 
	db.collectionName.find();
	db.collectionName.find({field: value});

*/	
db.users.find({"lastName":"Doe"});
db.users.find({"lastName":"Doe","age": 25}).pretty();

db.users.find({"_id":ObjectId("61760aeefb0f51759c71652c")});

// Updating Documents (Update) Operation
// SYNTAX: db.collectionName.updateOne({criteria}, {$set: {field: value}});


// Update one document
db.users.updateOne(
    { "firstName": "Test" },
    {
    
        $set: {
    "firstName": "Bill", 
        "lastName": "Gates",
        "age": 65,
        "contact": {
            "phone": "09170123465",
            "email": "bill@mail.com"
        },
        "courses": ["PHP", "Laravel", "HTML"],
        "department": "none"
            }
    }
 );

// Update multiple documents
db.users.updateMany(
    { "department": "none" },
    {
        $set: {"department": "HR"}
    }

 );


// Deleting Documents (Delete) Operation
// SYNTAX: db.collectionName.deleteOne({criteria}); - deleting a single document

// Create a single document with one field
db.users.insert({
    "firstName": "test"
});

// Delete one document
db.users.deleteOne({
    "firstName": "test"
});

// Update multiple documents where lastName is "Doe"
db.users.updateMany(
    {"lastName": "Doe"},
    {
	      $set: {"department": "Operations"}
	}
);

// Delete multiple
db.users.deleteMany(
    {"department": "Operations"}
);

// Advance Query

// Query and Embedded Document
db.users.find({
	"contact": {
		"phone": "09170123465",
		"email": "bill@mail.com"
	}
});

// Querying an Array without a specific order of elements
db.users.find({ "courses": { $all: ["React", "Python"]} });